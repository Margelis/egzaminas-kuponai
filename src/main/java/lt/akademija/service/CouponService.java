package lt.akademija.service;

import java.util.List;

import lt.akademija.dao.CouponDao;
import lt.akademija.entities.Coupon;

public class CouponService {
	private CouponDao couponDao;

	public CouponDao getCouponDao() {
		return couponDao;
	}

	public void setCouponDao(CouponDao couponDao) {
		this.couponDao = couponDao;
	}

	public void save(Coupon newCoupon){
		couponDao.save(newCoupon);
	}
	
	public void delete(Coupon coupon){
		couponDao.delete(coupon);
	}
	
	public List<Coupon> findAll(){
		return couponDao.findAll(); 
	}
	
	public Coupon findById(Integer id){
		return couponDao.findById(id);
	}
}
