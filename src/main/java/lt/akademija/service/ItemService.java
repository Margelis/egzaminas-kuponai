package lt.akademija.service;

import java.util.List;

import lt.akademija.dao.ItemDao;
import lt.akademija.entities.Item;

public class ItemService {
	private ItemDao itemDao;

	public void save(Item department) {
		itemDao.save(department);
	}

	public void delete(Item department) {
		itemDao.delete(department);
	}

	public List<Item> findAll() {
		return itemDao.findAll();
	}

	public Item findById(long id) {
		return itemDao.findById(id);
	}

	public List<Item> findCouponItems(long id) {
		return itemDao.findCouponItems(id);
	}

	public ItemDao getItemDao() {
		return itemDao;
	}

	public void setItemDao(ItemDao itemDao) {
		this.itemDao = itemDao;
	}

}
