package lt.akademija.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lt.akademija.entities.Coupon;

public class CouponDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}
	
	public void save(Coupon newCoupon) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newCoupon))
			newCoupon = em.merge(newCoupon);
		em.persist(newCoupon);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Coupon coupon) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		coupon = em.merge(coupon);
		em.remove(coupon);
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Coupon> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Coupon> cq = cb.createQuery(Coupon.class);
			Root<Coupon> root = cq.from(Coupon.class);
			cq.select(root); // we select entity here
			TypedQuery<Coupon> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public Coupon findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Coupon> couponQuery = em.createQuery("SELECT p From Coupon p WHERE p.id = :id", Coupon.class);
		couponQuery.setParameter("id", id);
		couponQuery.setMaxResults(1);
		return couponQuery.getSingleResult();

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
}
