package lt.akademija.model;

import java.io.Serializable;

import lt.akademija.entities.Coupon;

public class CouponModel implements Serializable{

	private static final long serialVersionUID = -8563883356667276555L;
	private Coupon currentCoupon;
	
	public Coupon getCurrentCoupon() {
		return currentCoupon;
	}
	public void setCurrentCoupon(Coupon currentCoupon) {
		this.currentCoupon = currentCoupon;
	}

	
}
