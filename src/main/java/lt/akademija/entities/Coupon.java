package lt.akademija.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Coupon implements Serializable {
	
	private static final long serialVersionUID = -1521832055830929022L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date date;
	private String seller;
	private String buyer;
	
	@OneToMany(mappedBy = "coupon", fetch = FetchType.EAGER, orphanRemoval=true)
	private List<Item> items;
	
	public Coupon(){};
	
	public double getTotalPriceLT(){
		double price = 0;
		if(!items.isEmpty()){
			
			for(Item i : items){
				price += i.getPriceLT();
			}
		}
		return price;
	}

	public double getTotalPriceEur(){
		double price = 0;
		if(!items.isEmpty()){
			
			for(Item i : items){
				price += i.getPriceEur();
			}
		}
		return price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
