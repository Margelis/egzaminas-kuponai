package lt.akademija.controllers;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import lt.akademija.entities.Item;
import lt.akademija.model.CouponModel;
import lt.akademija.model.ItemModel;
import lt.akademija.service.ItemService;

public class ItemController {
	private static final Logger log = LogManager.getLogger(ItemController.class.getName());
	private ItemModel itemModel;
	private ItemService itemService;

	private CouponModel couponModel;

	public void create() {
		itemModel.setCurrentItem(new Item());
	}

	public void save() {
		Item item = itemModel.getCurrentItem();
		item.setCoupon(couponModel.getCurrentCoupon());

		itemService.save(item);
		itemModel.setCurrentItem(null);
		log.info("saved");
	}

	public void cancel() {
		itemModel.setCurrentItem(null);
	}

	public void overview(Item item) {
		itemModel.setCurrentItem(item);
	}

	public List<Item> findCouponItems(long id) {
		return itemService.findCouponItems(id);
	}

	public void delete(Item item) {
		itemService.delete(item);
	}

	public ItemModel getItemModel() {
		return itemModel;
	}

	public void setItemModel(ItemModel itemModel) {
		this.itemModel = itemModel;
	}

	public ItemService getItemService() {
		return itemService;
	}

	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}

	public CouponModel getCouponModel() {
		return couponModel;
	}

	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}


}
