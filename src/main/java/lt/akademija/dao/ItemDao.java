package lt.akademija.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lt.akademija.entities.Item;

public class ItemDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}
	
	public void save(Item item) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(item))
			item = em.merge(item);
		em.persist(item);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Item item) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		item = em.merge(item);
		em.remove(item);
		em.getTransaction().commit();
		em.close();
	}
	
	public List<Item> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Item> cq = cb.createQuery(Item.class);
			Root<Item> root = cq.from(Item.class);
			cq.select(root); // we select entity here
			TypedQuery<Item> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Item> findCouponItems(long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Item> itemQuery = em.createQuery("SELECT p From Item p WHERE p.coupon.id = :id", Item.class);
		itemQuery.setParameter("id", id);
		
		return itemQuery.getResultList();
	}
	
	public Item findById(long id) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Item> itemQuery = em.createQuery("SELECT p From Item p WHERE p.id = :id", Item.class);
		itemQuery.setParameter("id", id);
		itemQuery.setMaxResults(1);
		return itemQuery.getSingleResult();

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
}
