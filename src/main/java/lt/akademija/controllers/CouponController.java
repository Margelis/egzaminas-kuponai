package lt.akademija.controllers;

import java.util.Date;
import java.util.List;

import lt.akademija.entities.Coupon;
import lt.akademija.model.CouponModel;
import lt.akademija.service.CouponService;

public class CouponController {
	
	private CouponModel couponModel;
	private CouponService couponService;
	
	public void create(){
		couponModel.setCurrentCoupon(new Coupon());	
	}
	
	public void save(){
		Coupon coupon = couponModel.getCurrentCoupon();
		coupon.setDate(new Date());
		couponService.save(coupon);
		couponModel.setCurrentCoupon(null);
	}
	
	public void delete(Coupon coupon){
		couponService.delete(coupon);
	}
	
	public void cancel(){
		couponModel.setCurrentCoupon(null);
	}
	
	public void overview(Coupon coupon){
		couponModel.setCurrentCoupon(coupon);
	}
	
	public List<Coupon> findCoupons(){
		return couponService.findAll();
	}

	public CouponModel getCouponModel() {
		return couponModel;
	}

	public void setCouponModel(CouponModel couponModel) {
		this.couponModel = couponModel;
	}

	public CouponService getCouponService() {
		return couponService;
	}

	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}
	

}
