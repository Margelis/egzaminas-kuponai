package lt.akademija.model;

import java.io.Serializable;

import lt.akademija.entities.Item;

public class ItemModel implements Serializable {
	
	private static final long serialVersionUID = -2640514586827666915L;
	private Item currentItem;
	
	public Item getCurrentItem() {
		return currentItem;
	}
	public void setCurrentItem(Item currentItem) {
		this.currentItem = currentItem;
	}


	
}
